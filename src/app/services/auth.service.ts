import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { auth } from 'firebase/app';
import { UserInterface } from '../models/user';
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object

import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';
import * as firebase from 'firebase';
import { NgModule } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private afsAuth: AngularFireAuth,
              private afs: AngularFirestore,
    ) {}

  loginFacebookUser() {
    return this.afsAuth.auth
      .signInWithPopup(new auth.FacebookAuthProvider())
      .then((credential) => this.updateUserData(credential.user));
  }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth
        .createUserWithEmailAndPassword(email, pass)
        .then((userData) => {
          console.log(userData);

          resolve(userData);
          // this.updateUserData(userData.user);
        })
        .catch((err) => console.log(reject(err)));
    });
  }

  loginUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth.auth
      .signInWithEmailAndPassword(email, pass)
      .then((userData) => {
        resolve(userData);
        // this.updateUserData(userData.user);

        console.log(userData);
      })
      .catch((err) => console.log(reject(err)));
    });
  }

  loginGoogleUser() {
    return this.afsAuth.auth
      .signInWithPopup(new auth.GoogleAuthProvider())
      .then((credential) => this.updateUserData(credential.user));
  }

  logoutUser() {
    return this.afsAuth.auth.signOut();
  }

  isAuth() {
    return this.afsAuth.authState.pipe(map((auth) => auth));
  }

  private updateUserData(user) {
    /**
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      roles: {
        editor: true
      }
    }
    return userRef.set(data, { merge: true })

	*/
    return false;
  }

  isUserAdmin(userUid) {
     return this.afs.doc<UserInterface>(`users/${userUid}`).valueChanges();
  }
}
