import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  OnChanges,
} from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { CategoryInterface } from '../../models/category';
import { VendorInterface } from 'src/app/models/vendor';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css'],
})
export class AddItemComponent implements OnInit {
  available: any;
  description: any;
  image: any;
  name: any;
  price: any;
  stock: any;
  categories: any;
  percent: any;
  saleprice: any;
  sub_category: any;
  vendor: any;
  status: any;
  sort: any;
  feature: any;
  new_arrival: any;
  color: any;
  size: any;

  colors: Array<string> = [];
  sizes: Array<string> = [];
  category_details: any;

  categoryList: any;
  sub_categoryList: any;
  vendorList: any;

  isVendor: any;
  user: any;

  private CategoryInterface: CategoryInterface[];
  public isAdmin: any = null;

  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router,
  ) { }
  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }

  ngOnInit() {
    this.checkVendor();
    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        // Using snapshotChanges() method to retrieve list of data along with metadata($key)
        this.categoryList = [];
        this.sub_categoryList = [];
        categories.forEach((item) => {

          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.categoryList.push(a as CategoryInterface);
        });
      });

    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendors) => {
        this.vendorList = [];
        vendors.forEach((item) => {

          let b = item.payload.toJSON();
          b['$key'] = item.key;

          this.vendorList.push(b as VendorInterface);
        });
      });
  }

  onItemAddSubmit() {
    // this.firebaseService
    //   .getCategoryDetails(this.categories)
    //   .snapshotChanges()
    //   .subscribe((category) => {
    //     this.category_details = [];

    //     let res = category.payload.toJSON();

    //     if (category.key != null || category.key != 'null') {
    //       res['$key'] = category.key;
    //     }

    //     console.log(category);

    //     this.category_details = res as CategoryInterface;

    //     console.log(this.category_details);
    if (this.user.roles.admin === true) {
      let item = {
        available: this.available,
        description: this.description,
        name: this.name,
        price: this.price,
        stock: this.stock,
        categories: this.categories,
        percent: this.percent,
        saleprice: this.saleprice,
        sub_category: this.sub_category ? this.sub_category : '',
        vendor: this.vendor,
        image: this.image,
        feature: this.feature,
        new_arrival: this.new_arrival,
        sort: this.sort,
        status: this.status,
        size: this.sizes,
        color: this.colors,
      };

      this.firebaseService.addItem(item);
      this.router.navigate(['items']);
    } else if (this.user.roles.vendor === true) {
      this.vendor = this.user.ven_key;
      let item = {
        available: this.available,
        description: this.description,
        name: this.name,
        price: this.price,
        stock: this.stock,
        categories: this.categories,
        percent: this.percent,
        saleprice: this.saleprice,
        sub_category: this.sub_category ? this.sub_category : '',
        vendor: this.vendor,
        image: this.image,
        feature: this.feature,
        new_arrival: this.new_arrival,
        sort: this.sort,
        status: this.status,
        size: this.sizes,
        color: this.colors,
      };

      this.firebaseService.addItem(item);
      this.router.navigate(['items']);
    }
  }

  setParentCategory(cat_id) {
    console.log(cat_id);
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
  }

  addColor(color) {
    console.log(color);
    let index = this.colors.findIndex(x => x == color)
    console.log(index);

    if (index === -1) {
      this.colors.push(color);
      color = undefined;
      console.log(this.colors);
    } else if (index === 0) {
      return;
    }
  }
  removeColor(i) {
    this.colors.splice(i, 1);
  }

  addSize(size) {
    console.log(size);
    let index = this.sizes.findIndex(x => x == size)
    console.log(index);

    if (index === -1) {
      this.sizes.push(size);
      size = undefined;
      console.log(this.sizes);
    } else if (index === 0) {
      return;
    }
  }
  removeSize(i) {
    this.sizes.splice(i, 1);
  }

  checkVendor() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {
      this.isVendor = false;
    } else if (this.user.roles.vendor === true) {
      this.isVendor = true;
    }
  }
}
