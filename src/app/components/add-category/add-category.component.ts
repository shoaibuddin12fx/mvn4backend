import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';
import { CategoryInterface } from 'src/app/models/category';
import { CityInterface } from 'src/app/models/city';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],
})
export class AddCategoryComponent implements OnInit {
  cat_id: any;
  cat_name: any;
  res_name: any;
  image?: any;
  restaurants: any;
  parent_categories: any[] = [];
  status: any = 'active';

  restaurant: any;
  parent_category_id: any;
  cities: any;

  private RestaurantInterface: RestaurantInterface[];
  public isAdmin: any = null;

  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router
  ) {}
  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }

  ngOnInit() {
    this.firebaseService
      .getRestaurants()
      .snapshotChanges()
      .subscribe((restaurants) => {
        // Using snapshotChanges() method to retrieve list of data along with metadata($key)
        this.restaurants = [];

        restaurants.forEach((item) => {

          const a = item.payload.toJSON();
          a['$key'] = item.key;

          console.log(a);

          this.restaurants.push(a as RestaurantInterface);
        });
      });
          this.firebaseService
            .getCities()
            .snapshotChanges()
            .subscribe((cities) => {
              // Using snapshotChanges() method to retrieve list of data along with metadata($key)
              this.cities = [];
              cities.forEach((item) => {
                const b = item.payload.toJSON();
                b["$key"] = item.key;
                this.cities.push(b as CityInterface);
              });
            });

    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        this.parent_categories = [];

        categories.forEach((item) => {

          const b = item.payload.toJSON();
          b['$key'] = item.key;

          console.log(b);

          this.parent_categories.push(b as CategoryInterface);
        });
      });
  }

  onCategoryAddSubmit() {

    
    // this.firebaseService
    //   .getRestaurantDetails(this.res_name)
    //   .snapshotChanges()
    //   .subscribe((restaurant) => {
    //     this.restaurant = [];
    //     //  restaurant.forEach(item => {

    //     //  console.log(item);

    //     const res = restaurant.payload.toJSON();
    //     res['$key'] = restaurant.key;

    //     console.log(restaurant);

    //     this.restaurant = res as RestaurantInterface;
    //     // this.restaurant.push(res as RestaurantInterface);

        
        this.parent_category_id = this.parent_category_id ? this.parent_category_id : ""
        

        
        const category = {
          cat_id: this.cat_id,
          cat_name: this.cat_name,
          image: this.image,
          // res_name: this.res_name,
          // restaurant_name: this.restaurant.title,
          // restaurant_image: this.restaurant.firebase_url,
          // restaurant_lat: this.restaurant.lat,
          // restaurant_long: this.restaurant.long,
          // res_id: this.restaurant.$key,
          // user_id: this.restaurant.user_id,
          status: this.status,
          parent_category_id: this.parent_category_id,
        };

        console.log(category);
        this.firebaseService.addCategory(category);
        this.router.navigate(['categories']);

        // 	  });
      // });
  }
}
