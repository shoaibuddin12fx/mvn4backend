import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  OnChanges,
} from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { ItemInterface } from 'src/app/models/item';
import { DistrictInterface } from 'src/app/models/district';
import { CityInterface } from 'src/app/models/city';

import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { Capabilities } from 'protractor';
import { VendorInterface } from 'src/app/models/vendor';
@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css'],
})
export class AddOrderComponent implements OnInit {
  name: any;
  items: any;
  item_price: any;
  item_qty: any;
  ven_name: any;
  email: any;
  phone: any;
  city: any;
  district: any;
  shipping_fee: any;
  subtotal: any;
  total_amount: any;
  status: any;
  item_image: any;
  address: any;
  order_status: any;

  itemList: any;
  item_priceList: any;
  cityList: any;
  districtList: any;
  shipping_feeList: any;
  vendorList: any;
  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router
  ) {}
  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }
  ngOnInit() {
    this.firebaseService
      .getItems()
      .snapshotChanges()
      .subscribe((items) => {
        this.itemList = [];
        this.item_priceList = [];
        items.forEach((item) => {

          const a = item.payload.toJSON();
          a['$key'] = item.key;

          this.itemList.push(a as ItemInterface);
        });
      });

    this.firebaseService
      .getDistricts()
      .snapshotChanges()
      .subscribe((districts) => {
        this.districtList = [];
        this.cityList = [];
        districts.forEach((item) => {

          const b = item.payload.toJSON();
          b['$key'] = item.key;

          this.districtList.push(b as DistrictInterface);
        });
      });
    this.firebaseService
      .getCities()
      .snapshotChanges()
      .subscribe((cities) => {
        this.cityList = [];
        cities.forEach((item) => {

          const c = item.payload.toJSON();
          c['$key'] = item.key;

          this.cityList.push(c as CityInterface);
        });
      });
      this.firebaseService.getvendor().snapshotChanges().subscribe((vendors) =>{
        this.vendorList = [];
        vendors.forEach((item) => {

          const d = item.payload.toJSON();
          d['$key'] = item.key;

          this.vendorList.push(d as VendorInterface);
        });
      });
  }

  setItemDetails($event) {
    console.log($event.target.value);
    this.item_price = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).price;
    this.item_image = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).image_firebase_url;
    this.status = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).status;
  }

  setCityDetails($event) {
    console.log($event.target.value);
    this.shipping_fee = this.cityList.find(
      (y) => y.name == $event.target.value
    ).shippingfee;
  }

  addMethod() {
    this.subtotal = this.item_qty * this.item_price;
    this.total_amount = this.shipping_fee + this.subtotal;
  }

  onOrderAddSubmit() {
    const order = {
      name: this.name,
      items: this.items,
      item_price: this.item_price,
      item_qty: this.item_qty,
      ven_name: this.ven_name,
      email: this.email,
      phone: this.phone,
      city: this.city,
      district: this.district,
      shipping_fee: this.shipping_fee,
      subtotal: this.subtotal,
      total_amount: this.total_amount,
      status: this.status,
      address: this.address,
      order_status: this.order_status,
      image: this.item_image
    };

    this.firebaseService.addNewOrder(order);
    this.router.navigate(['orders']);
  }
}
