import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';

import { OrderInterface } from '../../models/order';
import { VendorInterface } from 'src/app/models/vendor';
import { DistrictInterface } from 'src/app/models/district';
import { CityInterface } from 'src/app/models/city';
import { OrderPipe } from 'ngx-order-pipe';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  orders: any;
  vendors: any;
  ven_key: any;
  user: any;
  cities: any;
  districts: any;
  filteredOrders: any;
  order: string;
  reverse: any;
  sortedCollection: any;
  isVendor = false;
  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private orderPipe: OrderPipe,
  ) {
    // this.sortedCollection = orderPipe.transform(this.orders, 'createdAt');
    // console.log(this.sortedCollection);
  }

  ngOnInit() {
    this.getCurrentUserOrders();
    this.getVendors();
    this.getcities();
    this.getdistricts();
  }

  getVendors() {
    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendors) => {
        this.vendors = [];

        vendors.forEach((item) => {
          let b = item.payload.toJSON();
          b['key'] = item.key;

          this.vendors.push(b as VendorInterface);
        });
      });
  }

  getdistricts() {
    this.firebaseService
      .getDistricts()
      .snapshotChanges()
      .subscribe((districts) => {
        this.districts = [];

        districts.forEach((item) => {
          let b = item.payload.toJSON();
          b['key'] = item.key;

          this.districts.push(b as DistrictInterface);
        });
      });
  }

  getcities() {
    this.firebaseService
      .getCities()
      .snapshotChanges()
      .subscribe((cities) => {
        this.cities = [];

        cities.forEach((item) => {
          let b = item.payload.toJSON();
          b['key'] = item.key;

          this.cities.push(b as CityInterface);
        });
      });
  }

  getVendorName($key) {
    if (this.vendors && $key) {
      return this.vendors.find((x) => x.$key == $key).ven_name;
    }
  }

  getCityName($key) {
    if (this.cities && $key) {
      return this.cities.find((x) => x.key == $key).name;
    }
  }

  getDistrictName($key) {
    if (this.districts && $key) {
      return this.districts.find((x) => x.key == $key).name;
    }
  }

  getCurrentUserOrders() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {

      this.firebaseService
        .getOrders()
        .snapshotChanges()
        .subscribe((orders) => {
          // Using snapshotChanges() method to retrieve list of data along with metadata($key)
          this.orders = [];
          orders.forEach((item) => {
            const a = item.payload.toJSON();
            a['$key'] = item.key;
            console.log(a);
            this.orders.push(a as OrderInterface);
          });
        });

    } else if (this.user.roles.vendor === true) {
      this.ven_key = this.user.ven_key;
      this.firebaseService
        .getOrders()
        .snapshotChanges()
        .subscribe((orders) => {
          this.orders = [];
          orders.forEach((item) => {
            const a = item.payload.toJSON();
            a['$key'] = item.key;
            console.log(a);
            Object.keys(a['vendors']).forEach((key) => {
              let vendors = (a['vendors'][key].vendor_id);
              console.log(vendors);
              console.log(this.ven_key);
              if (vendors == this.ven_key) {
                console.log('Vendor Order', a);
                this.orders.push(a as OrderInterface);
              }
            });
          });
        });
    }
  }


  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
}
