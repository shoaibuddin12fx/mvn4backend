import { Component, OnInit } from '@angular/core';
import { AboutInterface } from 'src/app/models/about';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-about',
  templateUrl: './edit-about.component.html',
  styleUrls: ['./edit-about.component.css']
})
export class EditAboutComponent implements OnInit {
  phone1: any;
  phone2: any;
  viber: any;
  facebook: any;
  youtube: any;
  whatsapp: any;
  aboutdesc: any;
  abouttitle: any;
  telegram: any;
  instagram: any;
  address: any;
  app_version: any;
  email: any;

  about: any;
  id: any;
  constructor(
    private firebaseService: FirebaseService,
    private router: Router
  ) { }

  ngOnInit() {
    this.firebaseService.getAbout().snapshotChanges().subscribe(about => {
      this.about = [];

      about.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;

        console.log(a);
        this.about.push(a as AboutInterface);

        this.phone1 = this.about[0].phone1;
        this.phone2 = this.about[0].phone2;
        this.viber = this.about[0].viber;
        this.facebook = this.about[0].facebook;
        this.youtube = this.about[0].youtube;
        this.whatsapp = this.about[0].whatsapp;
        this.aboutdesc = this.about[0].aboutdesc;
        this.abouttitle = this.about[0].abouttitle;
        this.telegram = this.about[0].telegram;
        this.instagram = this.about[0].instagram;
        this.address = this.about[0].address;
        this.app_version = this.about[0].app_version;
        this.email = this.about[0].email;

        this.id = this.about[0].$key;
      });
    });
  }

  onAboutEditSubmit() {
    let about = {
      abouttitle: this.abouttitle,
      aboutdesc: this.aboutdesc,
      facebook: this.facebook,
      whatsapp: this.whatsapp,
      viber: this.viber,
      telegram: this.telegram,
      instagram: this.instagram,
      youtube: this.youtube,
      phone1: this.phone1,
      phone2: this.phone2,
      address: this.address,
      app_version: this.app_version,
      email: this.email
    };
    console.log(about);
    this.firebaseService.updateAbout(this.id, about);
    this.router.navigate(['about']);
  }
}
