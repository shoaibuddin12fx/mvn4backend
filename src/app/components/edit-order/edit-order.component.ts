import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';
import { CategoryInterface } from '../../models/category';
import { OrderInterface } from '../../models/order';
import { DistrictInterface } from '../../models/district';
import { CityInterface } from '../../models/city';
import { ItemInterface } from '../../models/item';

import { ActivatedRoute, Params } from '@angular/router';
import { VendorInterface } from 'src/app/models/vendor';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css'],
})
export class EditOrderComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  id: any;
  name: any;
  items: any;
  item_price: any;
  item_qty: any;
  email: any;
  phone: any;
  city: any;
  district: any;
  shipping_fee: any;
  subtotal: any;
  total_amount: any;
  status: any;
  item_image: any;
  address: any;
  cityList: any;
  itemList: any;
  districtList: any;
  order_status: any;
  ven_name: any;

  restaurant: any;
  imageUrl: any;
  categories: any;

  order_id: any;
  order_details: any;
  checked: any;
  order: any;
  vendorList: any;

  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  private OrderInterface: OrderInterface[];

  cat_id: any;
  category_details: any;

  user_details: any;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    // Reteriving The List OF items From FireBase
    this.firebaseService
      .getItems()
      .snapshotChanges()
      .subscribe((items) => {
        this.itemList = [];
        items.forEach((item) => {
          console.log(item);

          const a = item.payload.toJSON();
          a['$key'] = item.key;

          this.itemList.push(a as ItemInterface);
        });
      });

    // Reterieving List Of Districts Form FireBase
    this.firebaseService
      .getDistricts()
      .snapshotChanges()
      .subscribe((districts) => {
        this.districtList = [];
        this.cityList = [];
        districts.forEach((item) => {
          console.log(item);

          const b = item.payload.toJSON();
          b['$key'] = item.key;

          this.districtList.push(b as DistrictInterface);
        });
      });

    // Retrieving List Of Cities From FireBase
    this.firebaseService
      .getCities()
      .snapshotChanges()
      .subscribe((cities) => {
        this.cityList = [];
        cities.forEach((item) => {
          console.log(item);

          const c = item.payload.toJSON();
          c['$key'] = item.key;

          this.cityList.push(c as CityInterface);
        });
      });

    // Retrieving List Off Vendors Form FireBase
    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendors) => {
        this.vendorList = [];
        vendors.forEach((item) => {
          const d = item.payload.toJSON();
          d['$key'] = item.key;

          this.vendorList.push(d as VendorInterface);
        });
      });

    // Setting The Values To Be Displayed
    console.log('Here is Edit Order Page');

    this.id = this.route.snapshot.params['id'];

    console.log(this.id);

    this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
      this.order = snapshot.val();

      console.log(this.order);

      this.name = this.order.name;
      this.items = this.order.items;
      this.item_price = this.order.item_price;
      this.item_qty = this.order.item_qty;
      this.ven_name = this.order.ven_name;
      this.phone = this.order.phone;
      this.city = this.order.city;
      this.district = this.order.distict;
      this.shipping_fee = this.order.shipping_fee;
      this.subtotal = this.order.subtotal;
      this.status = this.order.status;
      this.email = this.order.email;
      this.district = this.order.district;
      this.city = this.order.city;
      this.total_amount = this.order.total_amount;
      this.address = this.order.address;
      this.item_image = this.order.image;
      this.order_status = this.order.order_status;

      console.log(this.id);
    });
  }

  // Setting Item Details on CHnage Event
  setItemDetails($event) {
    console.log($event.target.value);
    this.item_price = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).price;
    this.item_image = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).image_firebase_url;
    this.status = this.itemList.find(
      (x) => x.$key == $event.target.value
    ).status;
  }

  // Setting City Details on CHnage Event
  setCityDetails($event) {
    console.log($event.target.value);
    this.shipping_fee = this.cityList.find(
      (y) => y.name == $event.target.value
    ).shippingfee;
  }

  // Creating A Method for Summin The Values
  addMethod() {
    this.subtotal = this.item_qty * this.item_price;
    this.total_amount = this.shipping_fee + this.subtotal;
  }

  // Updating Data in FirebAse
  onStatusOrderSubmit() {
    const order = {
      name: this.name,
      items: this.items,
      item_price: this.item_price,
      item_qty: this.item_qty,
      ven_name: this.ven_name,
      email: this.email,
      phone: this.phone,
      city: this.city,
      district: this.district,
      shipping_fee: this.shipping_fee,
      subtotal: this.subtotal,
      total_amount: this.total_amount,
      status: this.status,
      address: this.address,
      order_status: this.order_status,
      image: this.item_image,
    };
    this.firebaseService.updateOrder(this.id, order);
    this.router.navigate(['/orders']);
  }
}
