import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 

import { MonthReportInterface } from '../../models/monthreport'; 

import { RestaurantOwnerInterface } from '../../models/restaurantowner'; 

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.css']
})
export class TaxComponent implements OnInit {
	
	available:any;
	category:any;
	description:any;
	image:any;
	name:any;
	price:any;
	stock:any;
	categories : any;
	percent: any;
	sandbox: any;
	production: any;
	publishable: any;
	secret: any;
	tax: any;

  constructor(private firebaseService:FirebaseService, private authService: AuthService , 
  private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }
  
   onTaxConfiguration(){
	
	  

		 let tax= {
			  percent: this.percent,
		  }


           this.firebaseService.addTaxConfiguration(tax);
          	  
	 
	 this.router.navigate(['/restaurants']);
  
  
  
  } 

}
