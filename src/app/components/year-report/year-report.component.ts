import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant'; 

import { MonthReportInterface } from '../../models/monthreport'; 

import { RestaurantOwnerInterface } from '../../models/restaurantowner'; 

@Component({
  selector: 'app-year-report',
  templateUrl: './year-report.component.html',
  styleUrls: ['./year-report.component.css']
})
export class YearReportComponent implements OnInit {
	
	
  todays: any;
  lastYear: any;
  lastMonth: any;
  yesterday: any;
  thisYear: any;
  thisMonth: any;
  thisDay: any;
  
  orderToday: any;
  results: any;
  result: any;
  
  user_details: any;
  
  restaurants : any;
  restaurant2: any;
  
  variableStorage: any;
  
  loopFinished: boolean = false;
	
  private books: BookInterface[];
  
  private RestaurantInterface: RestaurantInterface[];  
  private MonthReportInterface: MonthReportInterface[];  
  private RestaurantOwnerInterface: RestaurantOwnerInterface[];  
  
  public isAdmin: any = null;
  public userUid: string = null;

   constructor(private firebaseService:FirebaseService, private authService: AuthService , private router: Router){ }

  ngOnInit() {
	  
	  
			let today = new Date();
			this.lastYear = today.getFullYear() - 1;
			this.lastMonth = (today.getMonth() > 0) ? today.getMonth() : 12;
			this.yesterday = new Date(Date.now() - 86400000);
			this.thisYear = today.getFullYear();
			this.thisMonth = today.getMonth() + 1;
			this.thisDay = today.getDate();
			
						  
							  this.result = [];
							  
							  this.results = [];
							  
							  this.variableStorage = [];
							  
			
						this.firebaseService.getYearOrder(this.thisYear).snapshotChanges().subscribe(orderToday =>{
							  console.log(orderToday);
							 // this.result = orderToday;
							  
							 
							
							  orderToday.forEach(item => {
								  
								  console.log(item);
								  
							
											let a = item.payload.toJSON(); 
											a['$key'] = item.key;
								
											console.log(a);
								
											this.results.push(a as MonthReportInterface);
								
								
								
							  });
						
							  
							  
							  this.loopFinished = true;
							  
							  
							  
								  
								if(this.loopFinished == true){
									
											 this.results.forEach(items => {
																	console.log(items);
																	
																	
																	
																	this.firebaseService.getRestaurantOwnerInfo(items.restaurantOwnerId).snapshotChanges().subscribe(ownerInfo => {
																	  
																	 
																	  
																	  let res = ownerInfo.payload.toJSON(); 
																	  
																	  console.log(res);
																
																	  
																	  this.variableStorage = res as RestaurantOwnerInterface;
																	  
																	  items.email = this.variableStorage.email;
																	  items.displayName = this.variableStorage.displayName;
																	  items.lastName = this.variableStorage.lastName;
																	  items.phone = this.variableStorage.phone;
																	  
																	 
																	  
																	  this.result.push(items);
																	  
																		
																	});
																					
																
																	
																	console.log(this.result);  
													});  
											 
											   
										  }
							  
							  
						  });
	  
	
	
	  
  }

}
