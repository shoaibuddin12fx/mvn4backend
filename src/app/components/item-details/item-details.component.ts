import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';
import { CategoryInterface } from '../../models/category';
import { ItemInterface } from '../../models/item';

import { ActivatedRoute, Params } from '@angular/router';
import { VendorInterface } from 'src/app/models/vendor';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css'],
})
export class ItemDetailsComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  id: any;

  restaurant: any;
  imageUrl: any;
  categories: any;
  item: any;
  categoryList: any;
  vendorList: any;
  colors: Array<string> = [];
  sizes: Array<string> = [];
  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  private ItemInterface: ItemInterface[];

  public isAdmin: any = null;
  public userUid: string = null;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.firebaseService
      .getItemDetails(this.id)
      .snapshotChanges()
      .subscribe((item) => {
        this.item = [];
        //  restaurant.forEach(item => {

        //  console.log(item);

        const res = item.payload.toJSON();
        res['$key'] = item.key;

        console.log(item);

        this.item = res as ItemInterface;
        // this.restaurant.push(res as RestaurantInterface);

        console.log(this.item);
        Object.keys(this.item.size).forEach((key) => {
          this.sizes.push(this.item.size[key]);
        });
        Object.keys(this.item.color).forEach((key) => {
          this.colors.push(this.item.color[key]);
        });
        // 	  });
      });

    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((category) => {
        this.categoryList = [];

        category.forEach((item) => {

          const a = item.payload.toJSON();
          a['$key'] = item.key;

          this.categoryList.push(a as CategoryInterface);
        });
      });

      this.firebaseService.getvendor().snapshotChanges().subscribe((vendor) => {
        this.vendorList = [];

        vendor.forEach(item => {
          const b = item.payload.toJSON();
          b['$key'] = item.key;

          this.vendorList.push(b as VendorInterface);
        })
      })
  }

  onItemDelete() {
     this.firebaseService.deleteItem(this.id);
     this.router.navigate(['/items']);
  }

  getCategoryName($key) {
    console.log($key);
    return this.categoryList.find((x) => x.$key === $key).cat_name;
  }

  getvendorName($key) {
    console.log($key);
    return this.vendorList.find((x) => x.$key === $key).ven_name;
  }
}
