import { Component, OnInit } from "@angular/core";
import { FirebaseService } from "src/app/services/firebase.service";
import { AuthService } from "src/app/services/auth.service";
import { Router } from "@angular/router";
import { OrderInterface } from "src/app/models/order";
import { VendorInterface } from "src/app/models/vendor";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"],
})
export class ListComponent implements OnInit {
  users: any;
  vendors: any;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router
  ) {
    this.callList();
  }

  ngOnInit() {
    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendor) => {
        this.vendors = [];

        vendor.forEach((item) => {

          const b = item.payload.toJSON();
          b["$key"] = item.key;

          console.log(b);
          this.vendors.push(b as VendorInterface);
        });
      });
  }

  callList() {
    this.firebaseService
      .getUserList()
      .snapshotChanges()
      .subscribe((users) => {
        // Using snapshotChanges() method to retrieve list of data along with metadata($key)
        this.users = [];

        users.forEach((item) => {
          let a = item.payload.toJSON();
          a["$key"] = item.key;

          console.log(a);

          this.users.push(a as OrderInterface);
        });
      });

    console.log(this.users);
  }
  deleteUser(id) {
    this.firebaseService.deleteUser(id);
  }
  getVendorName($key) {
    console.log($key);
    return this.vendors.find((x) => x.$key == $key).ven_name;
  }
}
