import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserInterface } from 'src/app/models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    private firebaseService: FirebaseService,
    private authService: AuthService
  ) { }
  public email: string = '';
  public password: string = '';
  user: any;
  uid: any;
  userDetails: any;

  ngOnInit() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('User Id Is ' + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {

            this.userDetails = [];

            let a = user.payload.toJSON();

            if (!a) {
              this.afAuth.auth.signOut();
              this.router.navigate(['/']);
              return;
            }

            console.log(a);
            this.userDetails = a
            let _user = JSON.stringify(this.userDetails);
            localStorage.setItem('user', _user);

            if (this.userDetails.roles.admin == true) {
              alert('Welcome Super Admin');
              this.onLoginRedirect();
            } else if (this.userDetails.roles.vendor == true) {
              alert('Welcome Vendor');
              this.onLoginRedirect();
            }
          });
      } else {
        console.log('Failed');
      }
    });
  }
  onLogin(): void {
    this.authService
      .loginUser(this.email, this.password)
      .then((res) => {

        console.log(res);


      })
      .catch((err) => console.log('err', err.message));
  }

  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }

  onLogout() {
    this.authService.logoutUser();
  }
  onLoginRedirect(): void {
    this.router.navigate(['items']);
  }
}
