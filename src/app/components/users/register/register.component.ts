import { UserInterface } from "./../../../models/user";
import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
import { AngularFireStorage } from "@angular/fire/storage";
import { finalize } from "rxjs/operators";
import { Observable } from "rxjs/internal/Observable";

import * as firebase from "firebase";
import { FirebaseService } from "src/app/services/firebase.service";
import { VendorInterface } from "src/app/models/vendor";
import { forEach } from "@angular/router/src/utils/collection";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {

  isError = false;
  @ViewChild('imageUser') inputImageUser: ElementRef;

  public email = '';
  public password = '';
  public displayName = '';
  public lastName = '';
  public address = '';
  public cardnumber = '';
  public europeResult = '';
  public nation = '';
  public phone = '';
  public role = '';
  constructor(
    private router: Router,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private firebaseService: FirebaseService
  ) { }

  ven_name: any;
  venDeatails: any;
  vendor: any;
  ven_key: any;
  isVendor: boolean = false;
  show: false;
  users: any;
  phoneError = false;
  emailError = false;
  uploadPercent: Observable<number>;
  urlImage: Observable<string>;

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.firebaseService.getUserList().snapshotChanges().subscribe(res => {
      this.users = [];

      res.forEach(user => {
        let a = user.payload.toJSON();
        a['$key'] = user.key;

        this.users.push(a as UserInterface);

      });
    })
  }
  onUpload(e) {
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `uploads/profile_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();
  }

  onAddUser() {
    this.firebaseService
      .getVendorDetails(this.ven_key)
      .snapshotChanges()
      .subscribe((vendor) => {
        this.vendor = [];
        //  vendor.forEach(item => {

        //  console.log(item);

        let res = vendor.payload.toJSON();
        res["$key"] = vendor.key;

        console.log(vendor);

        this.vendor = res as VendorInterface;

        console.log(this.vendor);
      });
    var self = this;
    this.authService.registerUser(this.email, this.password).then((res) => {
      console.log(res);
      let user = res["user"];

      console.log(user);
      let r = this.role.toLowerCase() as string;
      let obj = {};
      obj[r] = true;

      firebase.database().ref("/users").child(user.uid).update({
        displayName: this.displayName,
        ownerId: user.uid,
        email: this.email,
        // address: this.address,
        phone: this.phone,
        lastName: this.lastName,
        facebook: false,
        ven_key: this.ven_key,
        ven_name: this.vendor.ven_name,
        // cardnumber: "XXXX-XXX",
        // europeResult: this.europeResult,
        // nation: this.nation,
        // facebooks: facebook,
        // instagram: instagram,
        // snapchat: snapchat,
        first: "true",
        status: "active",
        // photoURL: this.inputImageUser.nativeElement.value
        roles: obj,
      });

      console.log(this.displayName);
      console.log(this.lastName);
      console.log(this.address);
      console.log(this.phone);

      self.router.navigate(['users/list']);



      // user.updateProfile({
      //   displayName: this.displayName,
      //   photoURL: ""
      // }).then(() => {
      //   self.router.navigate(['users/list']);
      // }).catch((error) => console.log('error', error));
    });
  }

  onSelectVendor() {
    if (this.role === "VENDOR") {
      this.isVendor = true;
      this.firebaseService
        .getvendor()
        .snapshotChanges()
        .subscribe((vendor) => {
          this.venDeatails = [];

          vendor.forEach((item) => {

            const b = item.payload.toJSON();
            b["$key"] = item.key;

            this.venDeatails.push(b as VendorInterface);
          });
        });
    } else {
      this.isVendor = false;
    }
  }

  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log("err", err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log("err", err.message));
  }

  onLoginRedirect(): void {
    this.router.navigate(["admin/vendors"]);
  }

  checkPhoneNo() {
    let index = this.users.findIndex(x => x.phone == this.phone);

    if (index !== -1) {
      if (this.users[index].phone === this.phone) {
        this.phoneError = true;
      }
    } else {
      this.phoneError = false;
    }
  }

  checkEmailNo() {
    let index = this.users.findIndex(x => x.email == this.email);

    if (index !== -1) {
      if (this.users[index].email === this.email) {
        this.emailError = true;
      }
    } else {
      this.emailError = false;
    }
  }

}
