import { SliderInterface } from 'src/app/models/slider';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs/internal/Observable';
import { finalize } from 'rxjs/operators';
import { CategoryInterface } from 'src/app/models/category';

@Component({
  selector: "app-edit-slider",
  templateUrl: "./edit-slider.component.html",
  styleUrls: ["./edit-slider.component.css"],
})
export class EditSliderComponent implements OnInit {
  id: any;
  slider_name: any;
  image?: any;
  status: any = "active";
  slider: any;
  created_at: any;
  firebase_url: any;
  sort: any;
  type: any;

  sliderFolder: any;
  categories: any;

  @ViewChild("imageUser") inputImageUser: ElementRef;

  uploadPercent: number;
  urlImage: Observable<string>;

  constructor(
    private firebaseService: FirebaseService,
    private route: ActivatedRoute,
    private storage: AngularFireStorage,
    private router: Router
  ) {
    this.sliderFolder = "sliderimages";
  }

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];
    this.firebaseService
      .getSliderDetails(this.id)
      .snapshotChanges()
      .subscribe((slider) => {
        this.slider = {};
        //  restaurant.forEach(item => {

        console.log(slider);

        let res = slider.payload.toJSON();
        res["$key"] = slider.key;

        console.log(slider);

        this.slider = res as SliderInterface;
        //this.restaurant.push(res as RestaurantInterface);

        console.log(this.slider);

        this.slider_name = this.slider.slider_name;
        this.image = this.slider.image;
        this.status = this.slider.status;
        this.created_at = this.slider.created_at;
        this.firebase_url = this.slider.firebase_url;
        this.type = this.slider.type;
        this.sort = this.slider.sort;
        console.log(this.id);
      });

      this.firebaseService.getCategories().snapshotChanges().subscribe((category) => {
        this.categories = [];

        category.forEach(item => {

          const a = item.payload.toJSON();
          a['$key'] = item.key;

          this.categories.push(a as CategoryInterface)
        })
      })
  }

  onSliderEditSubmit() {
    console.log(this.image);

    if (
      !this.inputImageUser.nativeElement.value ||
      this.inputImageUser.nativeElement.value == undefined
    ) {
      console.log("inside");

      let slider = {
        slider_name: this.slider_name,
        image: this.image,
        firebase_url: this.firebase_url,
        status: this.status,
        sort: this.sort,
        created_at: this.created_at,
        type: this.type,
      };

      this.firebaseService.updateSlider(this.id, slider);

      this.router.navigate(["/sliders"]);
    }

    if (this.inputImageUser.nativeElement.value) {
      console.log("white");

      let slider = {
        slider_name: this.slider_name,
        status: this.status,
        sort: this.sort,
        created_at: this.created_at,
        image: this.inputImageUser.nativeElement.value,
        firebase_url: this.inputImageUser.nativeElement.value,
        type: this.type,

      };

      this.firebaseService.updateSliderWithImage(this.id, slider);
      this.router.navigate(["/sliders"]);
    }
  }

  onChange($event) {
    this.image = $event; // <--- File Object for future use.
  }

  onUpload(e) {
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.sliderFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    task.percentageChanges().subscribe(v => {
      this.uploadPercent = v;
    })


    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();

    alert("Please wait for uploading images");

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }
}
