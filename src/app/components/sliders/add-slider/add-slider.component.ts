import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { CategoryInterface } from 'src/app/models/category';

@Component({
  selector: 'app-add-slider',
  templateUrl: './add-slider.component.html',
  styleUrls: ['./add-slider.component.css']
})
export class AddSliderComponent implements OnInit {

  slider_name: any;
  image?: any;
  status: any = 'active';
  sort: any;
  type: any;

  categories: Array<any> = [];

  constructor(private firebaseService: FirebaseService, private router: Router) { }

  ngOnInit() {
    this.firebaseService.getCategories().snapshotChanges().subscribe((category) => {
      category.forEach(item => {

        const a = item.payload.toJSON();
        a['$key'] = item.key;
        console.log(a);

        this.categories.push(a as CategoryInterface);
      });

    });
  }

  onSliderAddSubmit() {


    let slider = {
      slider_name: this.slider_name,
      image: this.image,
      status: this.status,
      sort: this.sort,
      created_at: this.firebaseService.getTimeStamp(),
      type: this.type,

    };


    this.firebaseService.addSlider(slider);
    this.router.navigate(['sliders']);

  }

}
