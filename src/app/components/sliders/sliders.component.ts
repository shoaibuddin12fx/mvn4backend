import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { SliderInterface } from 'src/app/models/slider';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.css']
})
export class SlidersComponent implements OnInit {

  sliders: any;

  constructor(private firebaseService: FirebaseService, private authService: AuthService, private router: Router) { }

  ngOnInit() {

    this.firebaseService.getSliders().snapshotChanges().subscribe(slider => { // Using snapshotChanges() method to retrieve list of data along with metadata($key)
      this.sliders = [];
      slider.forEach(item => {

        console.log(item);
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        console.log(a);
        this.sliders.push(a as SliderInterface);

      })
    })


  }

  deleteSlider(id){
    this.firebaseService.deleteSlider(id);
    this.router.navigate(['/sliders'])
  }

}
