import { VendorInterface } from 'src/app/models/vendor';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';


@Component({
  selector: 'app-edit-vendor',
  templateUrl: './edit-vendor.component.html',
  styleUrls: ['./edit-vendor.component.css'],
})
export class EditVendorComponent implements OnInit {
  id: any;
  ven_name: any;
  shopname: any;
  latitude: any;
  longitude: any;
  address: any;
  phone1: any;
  phone2: any;
  email: any;
  logo?: any;
  date: any;
  status: any;
  firebase_url: any;
  vendor: any;

  vendorFolder: any;

  @ViewChild('logoUser') inputImageuser: ElementRef;

  uploadPercent: number;
  urlImage: Observable<string>;

  constructor(
    private firebaseService: FirebaseService,
    private storage: AngularFireStorage,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.vendorFolder = 'vendorimages';
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.firebaseService.getVendorDetails(this.id).snapshotChanges().subscribe(vendor => {
      this.vendor = {};

      console.log(vendor);

      let res = vendor.payload.toJSON();
      res['$key'] = vendor.key;

      console.log(vendor);

      this.vendor = res as VendorInterface;

      console.log(this.vendor);

      this.ven_name = this.vendor.ven_name;
      this.ven_name = this.vendor.ven_name;
      this.shopname = this.vendor.shopname;
      this.latitude = this.vendor.latitude;
      this.longitude = this.vendor.longitude;
      this.address = this.vendor.address;
      this.phone1 = this.vendor.phone1;
      this.phone2 = this.vendor.phone2;
      this.email = this.vendor.email;
      this.logo = this.vendor.logo;
      this.status = this.vendor.status;
      this.date = this.vendor.date,
      this.firebase_url = this.vendor.firebase_url;

      console.log(this.id);
    });
  }
  onVendorEditSubmit() {
    console.log(this.logo);
    if (
      !this.inputImageuser.nativeElement.value ||
      this.inputImageuser.nativeElement.value === undefined
    ) {
      console.log('inside');

      let vendor = {
        ven_name: this.ven_name,
        shopname: this.shopname,
        latitude: this.latitude,
        longitude: this.longitude,
        address: this.address,
        phone1: this.phone1,
        phone2: this.phone2,
        email: this.email,
        logo: this.logo,
        date: this.date,
        status: this.status,
        firebase_url: this.firebase_url,
      };

      this.firebaseService.updateVendor(this.id, vendor);
      this.router.navigate(['/vendors']);
    }

    if (this.inputImageuser.nativeElement.value) {
      console.log('white');

      let vendor = {
        ven_name: this.ven_name,
        shopname: this.shopname,
        latitude: this.latitude,
        longitude: this.longitude,
        address: this.address,
        phone1: this.phone1,
        phone2: this.phone2,
        email: this.email,
        date: this.date,
        status: this.status,
        logo: this.inputImageuser.nativeElement.value,
        firebase_url: this.inputImageuser.nativeElement.value,
      };

      this.firebaseService.updateVendorWithImage(this.id, vendor);
      this.router.navigate(['/vendors']);
    }
  }

  onChange($event) {
    this.logo = $event;
  }

  onUpload(e) {
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.vendorFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    
    task.percentageChanges().subscribe( v => {
      this.uploadPercent = v; 
    });
    
    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();

    alert('Please wait for uploading images');

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }
}
