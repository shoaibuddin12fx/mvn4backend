import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EachMonthReportComponent } from './each-month-report.component';

describe('EachMonthReportComponent', () => {
  let component: EachMonthReportComponent;
  let fixture: ComponentFixture<EachMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EachMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EachMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
