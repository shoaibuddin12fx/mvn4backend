import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveRestaurantsComponent } from './active-restaurants.component';

describe('ActiveRestaurantsComponent', () => {
  let component: ActiveRestaurantsComponent;
  let fixture: ComponentFixture<ActiveRestaurantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveRestaurantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveRestaurantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
