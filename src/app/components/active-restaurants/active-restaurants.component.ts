import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 

import { MonthReportInterface } from '../../models/monthreport'; 

import { RestaurantOwnerInterface } from '../../models/restaurantowner'; 

import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-active-restaurants',
  templateUrl: './active-restaurants.component.html',
  styleUrls: ['./active-restaurants.component.css']
})
export class ActiveRestaurantsComponent implements OnInit {

  restaurants : any;
	restaurant2: any;
	suspendedRestaurants: any;
	
	id: any;
	
	restaurant:any;
	imageUrl:any;
	categories: any;
	
  
  private RestaurantInterface: RestaurantInterface[];  
  private CategoryInterface: CategoryInterface[];  
  
  
  public isAdmin: any = null;
  public userUid: string = null;
  
  owner_id:any;
	order_details:any;
	user_details: any;
	
	


  constructor(private firebaseService:FirebaseService, private authService: AuthService , 
  private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
	  
	  this.id = this.route.snapshot.params['id'];
	  
	  this.firebaseService.getSuspendedRestaurantUser(this.id).snapshotChanges().subscribe(restaurants =>{
					console.log(restaurants);
					//this.categories = categories;
			  
					this.suspendedRestaurants = [];
					  restaurants.forEach(item => {
						  
						  console.log(item);
						  
					
						 let cat = item.payload.toJSON(); 
						cat['$key'] = item.key;
						
						console.log(cat);
						
						this.suspendedRestaurants.push(cat as RestaurantInterface);
						
						
						
					});
			  
		  });
  }
  
  
    onActiveRestaurant(restaurant){
	  

	  
	  console.log(this.owner_id);
	  
	  console.log(restaurant);
	  this.firebaseService.activeRestaurant(this.owner_id,restaurant);
	  
	  
	  
  }

}
