import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserInterface } from 'src/app/models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private afsAuth: AngularFireAuth,
    private firebaseService: FirebaseService,
    private router: Router
  ) { }
  public app_name: string = 'BookStore';
  public isLogged: boolean = false;
  public isLoggedVendor: any = false;
  public uid: string;
  userDetails: any;
  admin: boolean;
  buyer: boolean;
  user: any;
  ngOnInit() {
    this.checkCurrentUser();
  }

  getCurrentUser(uid) {
    return new Promise(resolve => {
      this.firebaseService
        .getUserDetail(uid)
        .snapshotChanges()
        .subscribe((user) => {

          this.userDetails = [];

          let a = user.payload.toJSON();

          this.userDetails = a;
          console.log(this.userDetails);

          if (this.userDetails.roles.admin == true) {
            resolve(false);
          } else if (this.userDetails.roles.vendor == true) {
            resolve(true);
          }
        });
    });
  }

  checkCurrentUser() {
    this.authService.isAuth().subscribe(async (auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('user logged', this.uid);
        this.isLoggedVendor = await this.getCurrentUser(this.uid);
        console.log(this.isLoggedVendor);
        this.isLogged = true;
      } else {
        console.log('NOT user logged');
        this.isLogged = false;
      }
    });
  }

  onLogout() {
    this.afsAuth.auth.signOut();
  }
  onLoginRedirect(): void {
    this.router.navigate(['restaurants']);
  }
}
