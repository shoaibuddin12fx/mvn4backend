import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';
import { ItemInterface } from '../../models/item';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  restaurants: any;
  restaurant2: any;

  items: any;
  ven_key: any;
  user: any;

  private RestaurantInterface: RestaurantInterface[];
  private ItemInterface: ItemInterface[];
  public isAdmin: any = null;
  public userUid: string = null;

  constructor(private firebaseService: FirebaseService, private authService: AuthService, private router: Router) { }

  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {
      this.firebaseService
        .getItems()
        .snapshotChanges()
        .subscribe(items => { // Using snapshotChanges() method to retrieve list of data along with metadata($key)
          this.items = [];
          items.forEach(item => {

            let a = item.payload.toJSON();
            a['$key'] = item.key;

            this.items.push(a as ItemInterface);
          })
        });
    } else if (this.user.roles.vendor === true) {
      this.ven_key = this.user.ven_key;
      this.firebaseService
        .getVendorItems(this.ven_key)
        .snapshotChanges()
        .subscribe((items) => {

          this.items = [];
          items.forEach((item) => {

            const a = item.payload.toJSON();
            a['$key'] = item.key;


            this.items.push(a as ItemInterface);
          });
        });
    }



  }



  onRestaurantDelete(id) {

    this.firebaseService.deleteRestaurant(id);

    this.router.navigate(['/restaurants']);
  }

  goToRestaurantDetails(restaurant) {
    console.log(restaurant);

    this.router.navigate(['/restaurants']);
  }

}
