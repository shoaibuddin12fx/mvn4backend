import { Component, OnInit } from "@angular/core";
import { DataApiService } from "../../services/data-api.service";
import { BookInterface } from "../../models/book";
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { UserInterface } from "../../models/user";
import { ViewChild, ElementRef, Input } from "@angular/core";
import { FirebaseService } from "../../services/firebase.service";
import { Router } from "@angular/router";

import { RestaurantInterface } from "../../models/restaurant";
import { CategoryInterface } from "../../models/category";
import { OrderInterface } from "../../models/order";
import { ItemInterface } from "src/app/models/item";

import { ActivatedRoute, Params } from "@angular/router";
import { VendorInterface } from "src/app/models/vendor";
import { CityInterface } from "src/app/models/city";
import { DistrictInterface } from "src/app/models/district";

@Component({
  selector: "app-order-details",
  templateUrl: "./order-details.component.html",
  styleUrls: ["./order-details.component.css"],
})
export class OrderDetailsComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  id: any;

  restaurant: any;
  imageUrl: any;
  categories: any;
  orders: any;

  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  private OrderInterface: OrderInterface[];

  cat_id: any;
  category_details: any;

  order_id: any;
  order_details: any;
  user_details: any;
  itemList: any;
  vendorList: any;
  item_img: any;
  cities: any;
  districts: any;
  userDetails: any;
  status: any;
  uid: any;
  user: any;
  index: any;
  ven_key: any;
  isIndex = false;
  isVendor = false;
  isAdmin = false;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getCurrentUserOrderItems();
    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendors) => {
        this.vendorList = [];
        vendors.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.vendorList.push(b as VendorInterface);
        });
      });

      this.firebaseService
      .getCities()
      .snapshotChanges()
      .subscribe((cities) => {
        this.cities = [];
        cities.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.cities.push(b as CityInterface);
        });
      });

      this.firebaseService
      .getDistricts()
      .snapshotChanges()
      .subscribe((districts) => {
        this.districts = [];
        districts.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.districts.push(b as DistrictInterface);
        });
      });

      this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        this.categories = [];
        categories.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.categories.push(b as CategoryInterface);
        });
      });
  }

  getCurrentUserOrderItems() {
    this.id = this.route.snapshot.params["id"];
    console.log(this.id);

    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {

      this.isAdmin = true;
      this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
        this.order_details = snapshot.val();
        console.log(this.order_details.items);
        this.itemList = [];
        this.order_details.items.forEach(element => {
          this.itemList.push(element);
        });
        console.log(this.itemList);
        console.log(this.order_details.vendors);
      });
    } else if (this.user.roles.vendor === true) {

      this.isVendor = true;
      this.firebaseService.getOrderDetail(this.id).on('value', async (snapshot) => {
        this.order_details = snapshot.val();
        this.itemList = [];
        this.ven_key = this.user.ven_key;

        this.index = this.order_details.vendors.findIndex(x => x.vendor_id === this.ven_key);
        this.isIndex = true;
        console.log(this.index);

        this.itemList = this.order_details.items.filter(x => x.vendor === this.ven_key);
        console.log(this.itemList);

      });
    }



  }
  getItemName($key) {
    return this.itemList.find((x) => x.$key == $key).name;
  }

  getCityName($key) {
    if ($key && this.cities) {
      return this.cities.find((x) => x.$key == $key).name;
    }
  }

  getDistrictName($key) {
    if ($key && this.districts) {
      return this.districts.find((x) => x.$key == $key).name;
    }
  }

  getCategoryName($key) {
    if ($key && this.categories) {
      return this.categories.find((x) => x.$key == $key).cat_name;
    }
  }

  getVendorName($key) {
    if ($key && this.vendorList) {
      return this.vendorList.find((x) => x.$key == $key).ven_name;
    }
  }

  updateVendorOrderStatus() {
    let obj = {
      id: this.id,
      index: this.index,
      status: this.status
    }

    this.firebaseService.updateVendorOrderStatus(obj);
  }
}
