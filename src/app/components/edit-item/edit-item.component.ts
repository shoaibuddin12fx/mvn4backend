import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import { FirebaseService } from '../../services/firebase.service';

import { RestaurantInterface } from '../../models/restaurant';
import { CategoryInterface } from '../../models/category';
import { ItemInterface } from '../../models/item';
import { VendorInterface } from 'src/app/models/vendor';

import { ActivatedRoute, Params } from '@angular/router';

import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css'],
})
export class EditItemComponent implements OnInit {
  id: any;
  name: any;
  description: any;
  available: any;
  price: any;
  stock: any;
  image: any;
  image_firebase_url: any;
  percent: any;
  saleprice: any;
  sub_category: any;
  vendor: any;
  status: any;
  sort: any;
  feature: any;
  new_arrival: any;
  categories: any;

  imageUrl: any;

  categoryList: any;
  sub_categoryList: any;
  vendorList: any;
  itemFolder: any;

  item: any;
  color: any;
  size: any;

  colors: Array<string> = [];
  sizes: Array<string> = [];

  isVendor: any;
  user: any;

  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private storage: AngularFireStorage
  ) {
    this.itemFolder = 'itemimages';
  }

  @Input() userUid: string;

  @ViewChild('imageUser') inputImageUser: ElementRef;

  uploadPercent: number;
  urlImage: Observable<string>;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      //  New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      //  Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    // this.btnClose.nativeElement.click();
  }

  ngOnInit() {
    console.log('Here is Edit Item Page');

    this.id = this.route.snapshot.params['id'];

    this.firebaseService
      .getItemDetails(this.id)
      .snapshotChanges()
      .subscribe((item) => {
        this.item = [];
        //   restaurant.forEach(item => {

        //   console.log(item);

        let res = item.payload.toJSON();
        res['$key'] = item.key;

        console.log(res);

        this.item = res as ItemInterface;

        console.log(this.item);

        this.name = this.item.name;
        this.description = this.item.description;
        this.available = this.item.available;
        this.price = this.item.price;
        this.saleprice = this.item.saleprice;
        this.stock = this.item.stock;
        this.status = this.item.status;
        this.new_arrival = this.item.new_arrival;
        this.sub_category = this.item.sub_category;
        this.categories = this.item.categories;
        this.percent = this.item.percent;
        this.sort = this.item.sort;
        this.vendor = this.item.vendor;
        this.feature = this.item.feature;
        this.saleprice = this.item.saleprice;
        this.image_firebase_url = this.item.image_firebase_url;

        Object.keys(this.item.size).forEach((key) => {
          this.sizes.push(this.item.size[key]);
        });
        Object.keys(this.item.color).forEach((key) => {
          this.colors.push(this.item.color[key]);
        });

        console.log(this.id);

        // 	  });
      });

    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        // Using snapshotChanges() method to retrieve list of data along with metadata($key)
        this.categoryList = [];
        this.sub_categoryList = [];
        categories.forEach((item) => {
          console.log(item);

          let a = item.payload.toJSON();
          a['$key'] = item.key;

          this.categoryList.push(a as CategoryInterface);
          this.sub_categoryList.push(a as CategoryInterface);
        });
      });

    this.firebaseService
      .getvendor()
      .snapshotChanges()
      .subscribe((vendors) => {
        this.vendorList = [];
        vendors.forEach((item) => {

          let b = item.payload.toJSON();
          b['$key'] = item.key;

          this.vendorList.push(b as VendorInterface);
        });
      });
  }

  onItemEditSubmit() {
    console.log(this.image);

    if (
      !this.inputImageUser.nativeElement.value ||
      this.inputImageUser.nativeElement.value == undefined
    ) {
      console.log('inside');

      if (this.user.roles.admin === true) {
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          new_arrival: this.new_arrival,
          sub_category: this.sub_category,
          categories: this.categories,
          saleprice: this.saleprice,
          vendor: this.vendor,
          percent: this.percent,
          sort: this.sort,
          feature: this.feature,
          image: this.image_firebase_url,
          image_firebase_url: this.image_firebase_url,
          size: this.sizes,
          color: this.colors,
        };
  
        this.firebaseService.updateItem(this.id, item);
  
        this.router.navigate(['/items']);

      } else if (this.user.roles.vendor === true) {
        this.vendor = this.user.ven_key;
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          new_arrival: this.new_arrival,
          sub_category: this.sub_category,
          categories: this.categories,
          saleprice: this.saleprice,
          vendor: this.vendor,
          percent: this.percent,
          sort: this.sort,
          feature: this.feature,
          image: this.image_firebase_url,
          image_firebase_url: this.image_firebase_url,
          size: this.sizes,
          color: this.colors,
        };
  
        this.firebaseService.updateItem(this.id, item);
  
        this.router.navigate(['/items']);
      }

    }

    if (this.inputImageUser.nativeElement.value) {
      console.log('white');
      if (this.user.roles.admin === true) {
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          new_arrival: this.new_arrival,
          sub_category: this.sub_category,
          categories: this.categories,
          vendor: this.vendor,
          percent: this.percent,
          saleprice: this.saleprice,
          sort: this.sort,
          feature: this.feature,
          image: this.inputImageUser.nativeElement.value,
          image_firebase_url: this.inputImageUser.nativeElement.value,
          size: this.sizes,
          color: this.colors,
        };
  
        this.firebaseService.updateItemWithImage(this.id, item);
  
         this.router.navigate(['/items']);

      } else if (this.user.roles.vendor === true) {
        this.vendor = this.user.ven_key;
        let item = {
          name: this.name,
          description: this.description,
          available: this.available,
          price: this.price,
          stock: this.stock,
          status: this.status,
          new_arrival: this.new_arrival,
          sub_category: this.sub_category,
          categories: this.categories,
          vendor: this.vendor,
          percent: this.percent,
          saleprice: this.saleprice,
          sort: this.sort,
          feature: this.feature,
          image: this.inputImageUser.nativeElement.value,
          image_firebase_url: this.inputImageUser.nativeElement.value,
          size: this.sizes,
          color: this.colors,
        };
  
        this.firebaseService.updateItemWithImage(this.id, item);
  
         this.router.navigate(['/items']);
      }

    }
  }

  onChange($event) {
    // let file = $event.target.files[0]; //  <--- File Object for future use.
    console.log($event);
    this.image = $event; //  <--- File Object for future use.
  }

  onUpload(e) {
    //  console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.itemFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    task.percentageChanges().subscribe( v => {
      this.uploadPercent = v;
    })
    
    
    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();

    alert('Please wait for uploading images');

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }

  setParentCategory(cat_id) {
    console.log(cat_id);
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
  }

  addColor(color) {
    console.log(color);
    let index = this.colors.findIndex(x => x == color)
    console.log(index);

    if (index === -1) {
      this.colors.push(color);
      color = undefined;
      console.log(this.colors);
    } else if (index === 0) {
      return;
    }
  }
  removeColor(i) {
    this.colors.splice(i, 1);
  }

  addSize(size) {
    console.log(size);
    let index = this.sizes.findIndex(x => x == size)
    console.log(index);

    if (index === -1) {
      this.sizes.push(size);
      size = undefined;
      console.log(this.sizes);
    } else if (index === 0) {
      return;
    }
  }
  removeSize(i) {
    this.sizes.splice(i, 1);
  }

  checkVendor() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {
      this.isVendor = false;
    } else if (this.user.roles.vendor === true) {
      this.isVendor = true;
    }
  }

}
