import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { NgForm } from '@angular/forms';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-about',
  templateUrl: './add-about.component.html',
  styleUrls: ['./add-about.component.css']
})
export class AddAboutComponent implements OnInit {

  phone1: any;
  phone2: any;
  viber: any;
  facebook: any;
  youtube: any;
  whatsapp: any;
  aboutdesc: any;
  abouttitle: any;
  telegram: any;
  instagram: any;
  address: any;
  app_version: any;
  email: any;

  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router) { }

  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }
  ngOnInit() {
  }

  onAboutAddSubmit() {
    let about = {
      abouttitle: this.abouttitle,
      aboutdesc: this.aboutdesc,
      facebook: this.facebook,
      whatsapp: this.whatsapp,
      viber: this.viber,
      telegram: this.telegram,
      instagram: this.instagram,
      youtube: this.youtube,
      phone1: this.phone1,
      phone2: this.phone2,
      address: this.address,
      app_version: this.app_version,
      email: this.email
    };
    console.log(about);
    this.firebaseService.addAbout(about);
    this.router.navigate(['about']);

  }
}
