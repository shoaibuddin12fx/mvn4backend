import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuspendRestaurantComponent } from './suspend-restaurant.component';

describe('SuspendRestaurantComponent', () => {
  let component: SuspendRestaurantComponent;
  let fixture: ComponentFixture<SuspendRestaurantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuspendRestaurantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspendRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
