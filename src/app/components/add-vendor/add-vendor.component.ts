import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { RestaurantInterface } from 'src/app/models/restaurant';
import { DataApiService } from 'src/app/services/data-api.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-add-vendor",
  templateUrl: "./add-vendor.component.html",
  styleUrls: ["./add-vendor.component.css"],
})
export class AddVendorComponent implements OnInit {
  ven_name: any;
  shopname: any;
  latitude: any;
  longitude: any;
  address: any;
  phone1: any;
  phone2: any;
  email: any;
  logo?: any;
  status: any;
  date: any;


  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router
  ) {}

  /* onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  } */

  ngOnInit() {}

  onVendorAddSubmit(){

    let vendor = {
      ven_name: this.ven_name,
      shopname: this.shopname,
      latitude: this.latitude,
      longitude: this.longitude,
      address: this.address,
      phone1: this.phone1,
      phone2: this.phone2,
      email: this.email,
      logo: this.logo,
      date: this.date,
      status: this.status,
    };

    this.firebaseService.addVendor(vendor);
    this.router.navigate(['vendors']);
  }
}


