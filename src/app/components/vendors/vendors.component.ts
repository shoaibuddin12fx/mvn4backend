import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { VendorInterface } from 'src/app/models/vendor';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.css']
})
export class VendorsComponent implements OnInit {

  vendors:any;

  constructor(private firebaseService: FirebaseService,
    private authService: AuthService,
    private router:Router) { }

  ngOnInit() {

    this.firebaseService.getvendor().snapshotChanges().subscribe(vendors =>{
      this.vendors = [];
      vendors.forEach(item => {

        console.log(item);
        let a = item.payload.toJSON();
        a['$key'] =item.key;
        console.log(a);
        this.vendors.push(a as VendorInterface);
      });

    })
  }

  deleteVendor(id) {
        this.firebaseService.deleteVendor(id);
        this.router.navigate(["/vendors"]);
  }

}
